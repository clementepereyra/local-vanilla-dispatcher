FROM centos/httpd

# This folder is required for some reason in the configuration. It could also
# just disabled for the `local` env, and then this instruction could be removed.
RUN mkdir -p /mnt/var/www/html

# Copy all files to the config-folder.
COPY ./ /etc/httpd/

# Copy Dispatcher
COPY ./dispatcher/dispatcher-apache2.4-4.2.3.so /etc/httpd/modules/
RUN ln -s /etc/httpd/modules/dispatcher-apache2.4-4.2.3.so /etc/httpd/modules/mod_dispatcher.so
