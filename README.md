# Vanilla Dispatcher Config

This is the vanilla AEM Dispatcher Apache configuration for 3|Share developers.

# Testing new settings with Docker

The repository includes a `Dockerfile` set up so that you can test the configuration within a container.

You need [Docker](https://www.docker.com/get-docker) to be installed.

## For local environment
Make sure you have your hosts file configured with the following rules:
```
127.0.0.1	localhost
127.0.0.1	author-localhost
```
Remember to set your Network IP for Author_IP and Publish_IP in conf/local-environment.conf because docker runs on a separate VM


## Building

To build the container:
```
# docker build -t vanilla-aem-apache .
```

### Running

To run the built container:
```
# docker run --rm -it -p 8080:80 --name vanilla-aem-apache vanilla-aem-apache
```

Next you browse to http://localhost:8080/

To get a login shell in the container:

```
# docker run --rm -dit -p 8080:80 --name vanilla-aem-apache vanilla-aem-apache
# docker exec -i -t vanilla-aem-apache /bin/bash
# docker container stop vanilla-aem-apache
```

### Clean up

To clean up what we just created:

```
# docker container rm vanilla-aem-apache
# docker image rm vanilla-aem-apache
```

## Server Apache version

The current server version on the dispatchers is:
```
Server version: Apache/2.4.6 (Red Hat Enterprise Linux)
Server built:   Mar  8 2017 05:09:47
Server's Module Magic Number: 20120211:24
Server loaded:  APR 1.4.8, APR-UTIL 1.5.2
Compiled using: APR 1.4.8, APR-UTIL 1.5.2
Architecture:   64-bit
Server MPM:     worker
  threaded:     yes (fixed thread count)
    forked:     yes (variable process count)
Server compiled with....
 -D APR_HAS_SENDFILE
 -D APR_HAS_MMAP
 -D APR_HAVE_IPV6 (IPv4-mapped addresses enabled)
 -D APR_USE_SYSVSEM_SERIALIZE
 -D APR_USE_PTHREAD_SERIALIZE
 -D SINGLE_LISTEN_UNSERIALIZED_ACCEPT
 -D APR_HAS_OTHER_CHILD
 -D AP_HAVE_RELIABLE_PIPED_LOGS
 -D DYNAMIC_MODULE_LIMIT=256
 -D HTTPD_ROOT="/etc/httpd"
 -D SUEXEC_BIN="/usr/sbin/suexec"
 -D DEFAULT_PIDLOG="/run/httpd/httpd.pid"
 -D DEFAULT_SCOREBOARD="logs/apache_runtime_status"
 -D DEFAULT_ERRORLOG="logs/error_log"
 -D AP_TYPES_CONFIG_FILE="conf/mime.types"
 -D SERVER_CONFIG_FILE="conf/httpd.conf"
```
